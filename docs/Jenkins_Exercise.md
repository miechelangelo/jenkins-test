## Bitbucket / Jenkins Java Exercise

Your goal in this exercise is to get Jenkins to run the tests for one of your own projects.

The general steps are listed below.

### Step 1) Ensure you Java project is in bitbucket.

If your LolChampions project is not in bitbucket, then add it now. The steps to add an EXISTING codebase to 
   bitbucket are:

``` text
    1) Create an EMPTY bitbucket repository in the bitbucket UI.
       Ensure you select NO Readme AND NO .gitignore (your project already has one).

    2) When you create the bitbucket repository it will say "You have created an empty repository".
       The bitbucket screen will have a command suggested at the bottom of the screen to "add a remote" to your project.
       You will run that command in a moment, so take note of it.

    3) On your VM (where your java project is) open gitbash and cd into the java project directory.
   
    4) If you don't already have a '.git' directory in your java folder then run 'git init' to initialise a local git repo

    5) Run the 'git remote add...' command that was suggested by bitbucket in step B)

    6) Now you can git add/commit/push as normal to get your code into your new bitbucket repo.
```


### Step 3) Add a Jenkinsfile to your codebase

This file will tell Jenkins "what to do" with your code.

You can take the example from here and edit it as needed:
https://bitbucket.org/fcallaly/mongodb-rest-pokemon/src/master/Jenkinsfile

You probably only need to edit the first line to set a variable to a name that represents your project (e.g. put your name somewhere in this name).

Try to understand the general steps that this Jenkinsfile is doing.


### Step 4) Create a Jenkins project

Create a folder in Jenkins for your work. Create a new item, select Pipeline item and give it a sensible name (perhaps the same name as the first line of the Jenkinsfile)

The only fields you'll likely want to change are:
    
    1) Give a description
    2) Check Build Triggers -> "Build when a change is pushed to Bitbucket"
    3) Select Pipeline -> "SCM"->"Git". Put your bitbucket Repository URL in the text box "Repository URL"
    4) IF you make your bitbucket project public, then you don't need to give Jenkins any credentials to access it.
    5) Save the Jenkins project



### Step 5) Test your build
On the jenkins page for your project click "Build Now". If there are errors try debugging what's happening with the "Console Output" screen.

If you get stuck, ask your instructor.


### Step 6) (Optional) Add a webhook to automatically trigger the build from a bitbucket push

In bitbucket, go to  "Repository Settings" -> "Webhooks" -> "Add webhook"

Set the title to Jenkins

Set the URL to http://devX.benivade.com:8080/bitbucket-hook/   (change X to your Jenkins number).

Check the checkbox "Active".

Check the checkbox "Skip certificate verification".

For now only select the "Push" trigger. However, note how you can control which git actions will send this webhook.

Click 'Save'

Now try pushing a trivial change to bitbucket. A few seconds later your build is queued in the Jenkins UI.


### Step 7) (Optional) Ensure jacoco is added to your project.
Verify that your project uses jacoco with this plugin section in pom.xml:

	    <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.7</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
 
Check that when your project is built by Jenkins, that Jenkins saves the code coverage results.
