package com.training.mongodbrest.service;

import com.training.mongodbrest.model.Pokemon;

import java.util.List;
import java.util.Optional;

public interface PokemonServiceInterface {

    List<Pokemon> findAll();

    Pokemon save(Pokemon pokemon);

    Optional<Pokemon> findById(String id);
    void delete(String id);
}
